#!/usr/bin/env python
# -*- coding: utf-8 -*-
import rmsd
import numpy as np
from ase.io import Trajectory
from ase.io.trajectory import TrajectoryWriter
from ase import Atoms
from ase.units import Debye
from tools.adf import m_water, ADF, qmmic
from tools.pesmakers import get_ooh
from tools.watercut import wcut 
from gridData import Grid
from scipy.ndimage import gaussian_filter
import glob
from ase.io import Trajectory 
import tqdm

adf = ADF([])

class AdvPDF:
    ''' Advanced Propability Distribution Functions of Water 
    
    Needs to:
        1. Sync up the following files:
            A.: QM dipole from .out files
            B.: .dpoles and .qpoles 
            C.: .traj
            
        2. Find rot matrix that rotates QM water back into SCME coordinate
           system
           
        3. Apply rot matrix to WHOLE Droplet. 
        
        4. 3D bin positions & pole norms of surrounding mols. 
        
        
        X1. Probably good to parallelize this at some point. 
    
    '''
    
    
    
    
    def __init__(self, qmidx=[0, 1, 2], nm=3, sample_within=3.35):
        self.qmidx = qmidx
        self.sw = sample_within
        self.nm = nm
        # init scme_geometry
        a = np.radians(104.52 / 2.)
        b = 0.9572
        self.scme_pos = np.array([[0, 0, 0],
                          [-b * np.sin(a), 0, -b * np.cos(a)],
                          [b * np.sin(a), 0, -b * np.cos(a)]])

        self.sdfs = {}
    
    
    def align(self, atoms_org, rot_idx=None):
        if rot_idx is None:
            rot_idx = self.qmidx
         
        atoms = atoms_org.copy()
        
        scme_pos = self.scme_pos
        org_pos = atoms[rot_idx].get_positions()
        scme_pos -= rmsd.centroid(scme_pos)
        trans = - atoms[0].position #rmsd.centroid(org_pos[0])
        new_pos = org_pos + trans
        
        U = rmsd.kabsch(scme_pos, new_pos)
        
        all_pos = atoms.get_positions() + trans
        
        aligned = np.dot(all_pos, U.T)
        
        
        atoms.set_positions(aligned)
        
        return atoms, U


    def readoutfile(self, outfile, eqsteps=0, stepsize=1):
        ''' Assuming (currently) that the .out contains a single water '''
        
        with open(outfile, 'r') as f:
            lines = f.readlines()
        
        # Run could have been stopped before SCF was converged, and thus the
        # number of positions will be greater than the number of dipoles in 
        # the file. So we start with the dipoles
        
        dipoles = np.array([[float(x.strip(',')) 
                            for x in y.split('(')[-1].split(')')[0].split()] 
                            for y in lines if 'Dipole moment:' in y])
        dipoles /= Debye
        
        steps = len(dipoles)
        
        pos_idx = []
        pos_ct  = 0
        for l, line in enumerate(lines):
            if 'Positions:' in line:
                pos_idx.append([l + j + 1 for j in range(3)])
                pos_ct += 1
            if pos_ct >= steps:
                break
            
        
        pos = np.zeros((3, 3, steps))
        for s, pidx in enumerate(pos_idx):
            mol = [lines[x].split() for x in pidx]
            for m in range(3):
                for xyz in range(3):
                    pos[m, xyz, s] = float(mol[m][2 + xyz]) 

                    
        
        return pos[:, :, eqsteps::stepsize], dipoles[eqsteps::stepsize]
    
    
    def outfile_align_export(self, outfile, eqsteps=0, stepsize=1):
        
        pos, dipoles = self.readoutfile(outfile, eqsteps, stepsize)
        
        outtrajname = outfile.replace('.out', '_aligned_QMpole.traj')
        writer = TrajectoryWriter(outtrajname, 'w')
        
        for s in range(np.shape(pos)[2]):
            atoms = Atoms('OHH', positions=pos[:, :, s])
            atoms, U = self.align(atoms)
            v = np.zeros((3, 3))
            v[0] = np.dot(dipoles[s], U.T)
            atoms.set_velocities(v)
            writer.write(atoms)
    
    def readpolefile(self, polefile, eqsteps=0, stepsize=1):
        if polefile.endswith('dpoles'):
            numrows = 5
        elif polefile.endswith('qpoles'):
            numrows = 20
        else:
            raise(IOError, 'Data type not understood')
        
        with open(polefile, 'r') as f:
            lines = f.readlines()
        
        steps = [x for x in lines if 'STEP: ' in x]
        if not steps:
            print('polefile seems empty:'+polefile.split('/')[-1] )
            return []
        try:
            nummols = int(lines[-1].split()[0]) + 1
        except:
            raise (IOError, 'Last line of file weird. Corrupt file? '+lines[-1])
        
        if eqsteps > len(steps):  # none of the traj is eq'ed
            return 0
        
        data = np.zeros((nummols, len(steps), numrows))
        if eqsteps > len(steps):
            print(polefile.split('/')[-1] + ' shorter than EQ-steps')
        
        ct_step = 0
        for l, line in enumerate(lines):
            if 'STEP: ' in line:
                for m, mol in enumerate(range(l + 1, l + 1 + nummols)):
                    d = np.array([float(x) for x in lines[mol].split()])
                    data[m, ct_step, :] = d
                
                ct_step += 1

        # prune away unequillibrated data etc:
        data = data[:, eqsteps::stepsize, :]
                    
        return data
            
    def readalldipolefiles(self, dipolefiles, eqsteps=0, stepsize=1):
      all_data = [self.readpolefile(pf, eqsteps, stepsize) for pf in dipolefiles]
      # remove empty trajectories before stacking
      all_data = [d for d in all_data if len(np.shape(d)) == 3]
      
      return np.hstack(all_data)


    def poles_into_trajs(self, data, trajfile, only_hb=False):
        qmidx = self.qmidx
        nm = self.nm
        
        try:
            traj = Trajectory(trajfile, 'r')
        except:
            print(trajfile + ' did not exist')
            return []
                
        outtrajname = trajfile.replace('.traj', '_aligned_poles.traj')
        if only_hb:
            outtrajname = trajfile.replace('.traj', '_aligned_poles_hb.traj')
        writer = TrajectoryWriter(outtrajname, 'w')
        
        if np.shape(data)[1] != len(traj):
            print(trajfile.split('/')[-1 ] + 
                  ' and pole file does not have equal steps!')
            return []
        
        for i, org_atoms in enumerate(traj):
            org_atoms.constraints = []
            
            #if only_hb:
            #    hb_idx = self.cutout_hbonded(org_atoms)
            #    org_atoms = org_atoms[qmidx + sum(hb_idx, [])]
    
            atoms, U = self.align(org_atoms, qmidx)
            poles = data[:, i, :]
            
            #if only_hb:
            #    p_hb = [int(x[0] / 3) for x in hb_idx]
            #    poles = poles[p_hb, :]
            
            dist_list = [x for x in range(0, len(atoms), nm)
                         if x not in qmidx[::3]]
            
            same_step = (atoms.get_distances(qmidx[0], dist_list) - 
                         poles[:, 1] < 1e-6).all()
            
            if not same_step:
                print('WARNING: DISTANCES IN POLE FILE AND TRAJ NOT EQUAL,\
                       Or the alignment failed')
            
            # some issues here if things should be more general
            p_idx = [int(x * 3) for x in poles[:, 0] + int(len(qmidx) / nm)] 
            poles_as_vel = np.zeros_like(atoms.get_positions())
            poles_as_vel[p_idx] = np.dot(poles[:, 2:], U.T)  # also rotated
            atoms.set_velocities(poles_as_vel)
            
            if only_hb:
               hb_idx = self.cutout_hbonded(atoms)
               atoms = atoms[qmidx + sum(hb_idx, [])]
               
            
            writer.write(atoms)

    def write_aligned_trajs(self, trajfile, maxmols=10, start=0, step=1):
        ''' Replacement for write_aligned_hbonded_trajs. 
            We should not _only_ write out hbonded mols, but ALL mols
            And _then_ look at 3D Spatial distributions... '''
        try:
            traj = Trajectory(trajfile, 'r')
        except:
            print(trajfile + ' did not exist')
            return []
        
        outtrajname = trajfile.replace('.traj', 
                                       '_aligned_max{0:02d}.traj'.format(maxmols))
        writer = TrajectoryWriter(outtrajname, 'w')
        
        for i, org_atoms in enumerate(traj):
            if i < start:
                continue
            elif np.mod(i, step) != 0:
                continue
            org_atoms.constraints = []

            # my water cutter only works for qm systems first apparently. 
            # REARRANGE:
            mask = np.zeros(len(org_atoms), bool)
            mask[self.qmidx] = True
            atoms = org_atoms[mask] + org_atoms[~mask]

            atoms, tags = wcut(atoms, maxmols, [0], [0, 1, 2])
            cutidx = np.where((tags == 2) | ( tags == 1))[0]
            atoms = atoms[cutidx]

            atoms, U = self.align(atoms, rot_idx=[0, 1, 2])
            atoms.positions -= atoms[0].position

            writer.write(atoms)

            
    def write_aligned_hbonded_trajs(self, trajfile, start=0):
        qmidx = self.qmidx
        try:
            traj = Trajectory(trajfile, 'r')
        except:
            print(trajfile + ' did not exist')
            return []
        
        outtrajname = trajfile.replace('.traj', '_aligned_hb.traj')
        writer = TrajectoryWriter(outtrajname, 'w')
        
        for i, org_atoms in enumerate(traj):
            if i < start:
                continue
            org_atoms.constraints = []
            atoms = qmmic(org_atoms, qmidx, self.nm)
            
            atoms, U = self.align(atoms, qmidx)
            hb_idx = self.cutout_hbonded(atoms)
            atoms = atoms[qmidx + sum(hb_idx, [])]
            
            # translate such that first atom is in (0, 0, 0)
            atoms.positions -= atoms[0].position
            writer.write(atoms)
            
    def cutout_hbonded(self, atoms):
        mols = (len(atoms)) // 3
        mol1 = atoms[self.qmidx]
        pos = atoms.get_positions().reshape((mols, 3, 3))
        allcom = np.dot(m_water, pos) / m_water.sum()
        mol1com = np.dot(m_water, mol1.positions) / m_water.sum()
        moldists = np.apply_along_axis(np.linalg.norm, 1, 
                                       allcom - mol1com)
        
        #  only look at closest waters 
        within = moldists <= self.sw
        w_idx, = np.where(within)
        
        b_idx = []
        for j in w_idx:
            if j in self.qmidx:
                continue
            
            mol2idx = range(j * 3, j * 3 + 3)
            mol2 = atoms[mol2idx]
        
            (don, acc, hd, 
            d_is_qm) = adf.water_dimer_identify(mol1, mol2, True, False)
            
            beta = np.abs(get_ooh(don + acc, 3, hd)[0])
            roo = adf.get_roo(don, acc)
            bonded = roo < self.sw - 0.00044 * (beta)**2

            if bonded:
                b_idx.append(list(range(j * self.nm, j * self.nm + self.nm)))
                
        return b_idx    


    def concatpos_for_sdf(self, inpstr, out_npy=None, step=1, trajstep=1):
        trajs = sorted(glob.glob(inpstr))
        pos = []
        for t, traj in enumerate(trajs):
            if t % trajstep != 0:
                continue
            traj = Trajectory(traj, 'r')
            for a, atoms in enumerate(traj):
                if a % step != 0:
                    continue
                pos.append(atoms.get_positions()[3:])
        
        if out_npy:
            np.save(out_npy, pos)

        return pos


    def sdf(self, atm_pos, bins=(27, 27, 27), x=10., y=10, z=10, num_mols=512, 
                    box=(24.84930047, 24.84930047, 24.84930047), idelta=0.1, sig=0.5,
                    outname='sdf'):
        ''' Export SDF, renormalized after H-bond filtering or cutting out N nearest.
            Should be run on list of atom positions per frame from trajs made with
            write_aligned_hbonded_trajs  / write_aligned_trajs 

            exports .dx files to be read by VMD / Chimerea '''

        rho = num_mols / (box[0] * box[1] * box[2])
        center_pos = self.scme_pos

        atoms = Atoms('OHH', positions=center_pos)

        self.sdf = {'O':None, 'H':None}

        for atm in ('O', 'H'):
            mask = np.zeros(len(atm_pos[0]), bool)
            mask[0::3] = (atm == 'O')
            for h in [1, 2]:
                mask[h::3] = (atm == 'H')
            
            A, edges = np.histogramdd(atm_pos[0][mask], 
                                      bins=bins, 
                                      range=((-x / 2., x / 2.),
                                             (-y / 2., y / 2.),
                                             (-z / 2., z / 2.)),
                                      density=False)

            dx = edges[0][1] - edges[0][0]
            dy = edges[1][1] - edges[1][0]
            dz = edges[2][1] - edges[2][0]

            A_all = np.zeros_like(A)

            for frame_pos in atm_pos:
                mask = np.zeros(len(frame_pos), bool)
                mask[0::3] = (atm == 'O')
                for h in [1, 2]:
                    mask[h::3] = (atm == 'H')

                A, edges = np.histogramdd(frame_pos[mask], 
                                          bins=bins, 
                                          range=((-x / 2., x / 2.),
                                                 (-y / 2., y / 2.),
                                                 (-z / 2., z / 2.)),
                                          density=False)
                
                num_As = len(frame_pos[0::3])
                if num_As == 0:
                    #print('ZERO MOLS, skipping step')
                    continue
                    
                norm =  np.sum(A * dx * dy * dz / num_As)
                
                A_all += A / (rho * norm)

            A_all /= len(atm_pos)
            
            g = Grid(A_all, edges=edges)
            g.export(outname + '_' + atm + '.dx', type='double')
            
            newedges=[np.arange(edges[0][0], edges[0][-1], idelta), 
                      np.arange(edges[1][0], edges[1][-1], idelta), 
                      np.arange(edges[2][0], edges[2][-1], idelta)]

            g.interpolation_spline_order = 3  # cubic
            gi = g.resample(newedges)
            gi.export(outname + '_' + atm + '_i{0:2.2f}'.format(idelta) +\
                      '.dx', type='double')

            si = gaussian_filter(gi.grid, sig)
            si = Grid(si, edges=gi.edges)
            si.export(outname + '_' + atm + '_g{0:2.2f}'.format(sig) +\
                      '.dx', type='double')

            self.sdfs[atm] = (g, gi, si)

