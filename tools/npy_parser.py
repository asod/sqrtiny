#!/usr/bin/env python2
# -*- coding: utf-8 -*-
"""
Read in npys, collect, rebin, analyze, etc. 

The analysis tools that require reorientation of molecules uses the 
RMSD package from https://github.com/charnley/rmsd by default.

You can disable this, but then the rotations assume SCME geometry, and will 
thus be less precise.


SCME-Specific functions:

More analysis funtions should adhere to the format:
    
def function(self, d, **kwargs):
    if d is None:
        return <NUMBER OF OUTPUT LISTS>
    
    variable_needed = kwargs['variable_needed']
    
    <do analysis>

    return [var1], [var2] 
Created on Thu Oct 18 11:40:12 2018

@author: Asmus O. Dohn
"""

from __future__ import print_function
import numpy as np
import glob 
import rmsd
from ase.units import Bohr
from math import factorial


def combs(n, k):
    N = factorial(n) / (factorial(k) * factorial(n-k))
    return N


''' Functions for rotating dipoles back into scme frame. '''
def ssc(v):
    return np.array([[0, -v[2], v[1]],
                     [v[2], 0, -v[0]],
                     [-v[1], v[0], 0]])

def RU(A, B):
    """ RU(B, A) * B = A 
    * = np.matmul(), please remember... """
    return np.identity(3) + ssc(np.cross(A,B)) +\
           np.matmul(ssc(np.cross(A,B)), ssc(np.cross(A,B)))*\
           (1-np.dot(A,B)) / np.linalg.norm(np.matmul(np.cross(A, B), np.cross(A, B)))



def rotate_pole(molpos, pole):
    """ I need to calculate the rotation matrix 
        that takes the molecule back to the reference 
        and apply that to the pole.  
        
        The original SCME frame for poles have center in COM 
        and Hs in ZX plane, we just use the O pos (its just a z translation 
        and we can start the vectors wherever we feel like anyway.)
    """
    v_scme = np.array([0., 0., - 0.9572 * np.cos(np.radians(104.52 / 2.))])
    d_scme = np.linalg.norm(v_scme)  # only works for unit vectors
    v_scme /= d_scme 
    
    v_oh = molpos[1] - molpos[0]
    v_hh = molpos[2] - molpos[1]
    v_mol = v_oh + 0.5 * v_hh
    d_mol = np.linalg.norm(v_mol)
    v_mol /= d_mol
    
    U = RU(v_mol, v_scme)  # gives the matrix that rotates the mol back to scme frame
    
    polelen = np.linalg.norm(pole)
    
    return np.matmul(U, pole/polelen)*polelen


def rmsd_rotate_pole(molpos, pole):
    ''' The same as rotate_pole , but with the rmsd tools.
        The rotate_pole in in principle assumes SCME internal geometry
        of both water molecules. This version finds the rotation matrix
        that gets the minimum RMSD between the two molecule geoms. 
    '''
    
    m1 = molpos
    spp = NPYParser()
    m2 = spp.scme_pos
    
    # center both in zero
    m1 -= rmsd.centroid(m1)
    m2 -= rmsd.centroid(m2)
    
    U = rmsd.kabsch(m1, m2)
    
    return np.dot(pole, U)

   
    
class NPYParser:
    """ Watercluster NPY data parser """     
    def __init__(self, npydir=None, nndist=3.2):
        self.npydir = npydir  # list of files or string to dir
        self.out = {}
        self.nndist = 3.2
        
        # init scme_geometry
        a = np.radians(104.52 / 2.)
        b = 0.9572
        self.scme_pos = np.array([[0, 0, 0],
                                  [-b * np.sin(a), 0, -b * np.cos(a)],
                                  [b * np.sin(a), 0, -b * np.cos(a)]])
        self.b1_scme = 0.9572
        self.b2_scme = 0.9572
        self.b3_scme = np.linalg.norm(self.scme_pos[1] - self.scme_pos[2])
        
        self.pure_ddes = []
        self.qp_static_norm = np.linalg.norm(np.array([1.9932, 
                                                      -1.85867,
                                                      -0.09665]))

        self.scme_dip = 0.72981

    def use_standard_functions(self):
        """ The list of standard """
        functions = [self.polediff, self.poleangle, self.pnormdiff,
                     self.pnormdiffpct, self.dens_and_qmnn,
                     self.bond_distortion,  self.delta_eint,
                     self.qm_dipdiff, self.rmsd_diff, self.rot_qmmm_dip,
                     self.rot_mmmm_dip, self.qpnormdiffpct]
        
        outnames = ['allpdiffs', 'allangles', 'allnorms', 'allnormspct', 
                    'allmindists', 'allscmelike_dens', 'allqm_nns',
                    'global_mindist',
                    'alldistortions', 'alldistortions_bonds', 
                    'alldeints', 'DDE',  # same thing but first is repeated pr pole
                    'allqmangle', 'allqmnormdiff', 'allrmsd', 'rot_qmmm_dip',
                    'rot_mmmm_dip', 'qpolenormdiffs'] 
        
        return functions, outnames        
        
        
    def get_outvars(self, out_functions, data, **kwargs):
        # initialize output lists
        this_geom_var = []
        for f in out_functions:
            for i in range(f(None)):
                this_geom_var.append([])
                
        this_geom_numqm = []
        this_geom_numqm_perpole = []
        
        qmmm_calcs = [k for k in list(data.keys()) if isinstance(k, int)]

        for c in sorted(qmmm_calcs):
            d = data[c]
            
            # Make number of QM mols from index
            numqm = len(d['qm_idx']) // 3
            this_geom_numqm.append(numqm)
            
            #qmmm_poles = (d['mm_dipoles'] + 0.5 * d['mm_dipolesQM'])
            
            molpos = d['positions']  # pos the same for all QM/MM confs
            mask = np.zeros(len(molpos), bool)
            mask[d['qm_idx']] = True  # so now the mm atoms are ~mask 
        
            if 'mm_dipoles' in d.keys():
                qmmm_poles = (d['mm_dipoles'] + 0.5 * d['mm_dipolesQM'])
                qmmm_qpoles = (d['mm_qpoles'] + 0.5 * d['mm_qpolesQM'])
            else:
                qmmm_poles = np.zeros((len(d['mm_positions']) // 3, 3))
                qmmm_qpoles = np.zeros((len(d['mm_positions']) // 3, 3))
            
            r_qmmm_poles = np.zeros_like(qmmm_poles)
        
            mm_molpos = d['positions'][~mask]
            for m, p in enumerate(qmmm_poles):
                m_pos = mm_molpos[m * 3:m * 3 + 3]
                r_qmmm_poles[m] = rmsd_rotate_pole(m_pos, qmmm_poles[m])            
            
            kwargs['qmmm_poles'] = qmmm_poles
            kwargs['qmmm_qpoles'] = qmmm_qpoles
            kwargs['r_qmmm_poles'] = r_qmmm_poles
            kwargs['mask'] = mask
            kwargs['DATA'] = data
                        
            # Making the data from the raw data.        
            ct = 0
            for i, f in enumerate(out_functions):
                fixed_data = f(d, **kwargs)
                nout = f(None)
                for j in range(nout):
                    this_geom_var[ct].extend(fixed_data[j])
                    ct += 1
                    
                
            # number of QM mols for each pole:
            this_geom_numqm_perpole.extend(np.zeros(len(qmmm_poles),
                                                    dtype=int) + numqm)
            
        return this_geom_var, this_geom_numqm, this_geom_numqm_perpole
        
        
    def update_outdict(self, var, varname):
        self.out[varname] = var
        
    
    def update_pure_differences(self, npy):
        qm = npy['QM']
        mm = npy['MM']
        nummols = len(npy['JOB_INFO']['atoms']) / 3
        
        diff = qm['energies']['eint'] - mm['energies']['eint']
        pure_dde = 1e3 * diff / nummols
        
        self.pure_ddes.append([pure_dde, nummols])
    

    def get_mm(self, data, npy):
        if 'dipoles' in data['MM'].keys():
            mm_poles = data['MM']['dipoles']
            mm_qpoles = data['MM']['qupoles']
        else:
            mm_poles = np.zeros((len(data['JOB_INFO']['atoms']) // 3, 3))
            mm_qpoles = np.zeros((len(data['JOB_INFO']['atoms']) // 3, 3))
        
        r_mm_poles = np.zeros_like(mm_poles)
        r_mm_qpoles = np.zeros_like(mm_qpoles)
        
        molpos = data[0]['positions']  # positions dont change... 
        for m, p in enumerate(mm_poles):
            # Rotate back
            m_pos = molpos[m * 3:m * 3 + 3]
            r_mm_poles[m] = rmsd_rotate_pole(m_pos, mm_poles[m])
            
            r_mm_qpoles[m] = rmsd_rotate_pole(m_pos, mm_qpoles[m])
                    
        self.mm_poles = mm_poles
        self.r_mm_poles = r_mm_poles
        self.mm_qpoles = mm_qpoles
        self.r_mm_qpoles = r_mm_qpoles
        
        this_mm_eint = data['MM']['energies']['eint']
        self.this_mm_eint = this_mm_eint
        
        
    def get_data(self, npydir=None, functions=None, outnames=None, 
                 npystring='*.npy'):
        
        self.pure_ddes = []
        
        if npydir is None:
            npydir = self.npydir

        if functions is None:
            functions, outnames = self.use_standard_functions()
            
        all_var = []
        all_tgnqm = []
        all_tgnpp = []
        self.all_num_cluster = []
        for f in functions:
            for i in range(f(None)):
                all_var.append([])
        
        if isinstance(npydir, str):
            npys = sorted(glob.glob(npydir+'/'+npystring)) 
        elif isinstance(npydir, list):
            npys = npydir
        else:
            raise TypeError('Input not understood')
        
        for npy in npys:
            if '4179_water2Cs' in npy: # Skip the dimer
                print('Skipping the dimer')
                continue
            
            #data = np.load(npy, encoding='latin1').item()
            data = np.load(npy, encoding='latin1', allow_pickle=True).item()
            
            if 'MM' not in data.keys():
                # Probably not done yet
                print('Skipping '+npy.split('/')[-1])
                continue
            
            self.num_cluster = len(data['JOB_INFO']['atoms']) // 3
            self.all_num_cluster.append(self.num_cluster)
            self.get_mm(data, npy)
            self.update_pure_differences(data)
            
            # process the data with each of the functions in the list 
            # of functions:
            tgv, tgnqm, tgnpp = self.get_outvars(functions, data)
            
            
            # append to the list containing data for all geometries
            ct = 0
            for i, f in enumerate(functions):
                nouts = f(None)
                for j in range(nouts):
                    all_var[ct].extend([tgv[ct]])
                    ct += 1
            all_tgnqm.append(tgnqm)
            all_tgnpp.append(tgnpp)
            
            
        # Save to self and to out dict. 
        self.all_var = all_var
        self.all_tgnqm = all_tgnqm
        self.all_tgnpp = all_tgnpp
        assert len(outnames) == len(all_var)
        
        for var, name in zip(all_var, outnames):
            self.update_outdict(var, name)
        
        self.update_outdict(all_tgnqm, 'allnumqm')
        self.update_outdict(all_tgnpp, 'allnumqmperpole')
        self.update_outdict(self.all_num_cluster, 'num_clusters')

        
    def rebin_pr_numqm(self, var): 
        rebinned_var = []
        
        rebinned_nc = []
        for nm in np.unique(self.all_num_cluster):
            # stack all values with the same total number of molecules
            nc_idx = np.where(np.array(self.all_num_cluster) == nm)[0]
            # only works if all runs are finished, so there's an equal amount
            # of datapoints in all runs with the same num_cluster
            # nm_var = np.stack(np.array(var)[nc_idx]).reshape(-1)
            nm_var = []
            nm_var.extend([var[x] for x in nc_idx])
            nm_var = np.array(sum(nm_var, []))
            
            # number of QM mols for this nm
            # same problem as above with this one:
            # nmnqm = np.stack(np.array(self.all_tgnpp)[nc_idx]).reshape(-1)
            nmnqm = []
            nmnqm.extend([self.all_tgnpp[x] for x in nc_idx])
            nmnqm = np.array(sum(nmnqm, []))
            
            # divide up again in number of QM
            pr_numqm_var = []
            pr_numqm_nc = []
            for x in np.unique(nmnqm):
                pr_numqm_var.append(nm_var[np.where(nmnqm == x)[0]])
                pr_numqm_nc.append(nm)
            
            rebinned_var.append(pr_numqm_var)
            rebinned_nc.append(pr_numqm_nc)
            
        self.rebinned_nc = rebinned_nc
        
                
        return rebinned_var

    
    def rebin_vectors_pr_numqm(self, var):
        rebinned_var = []
        for nm in np.unique(self.all_num_cluster):
             nc_idx = np.where(np.array(self.all_num_cluster) == nm)[0]
             selected_vars = np.array(var)[nc_idx]
             vector_stacked = sum(selected_vars, [])  # all vecs of nm
             
             # number of QM mols for this nm
             nmnqm = np.stack(np.array(self.all_tgnpp)[nc_idx]).reshape(-1)
             
             # rebin
             pr_num_qm_var = []
             for x in np.unique(nmnqm):
                 this_numqm = np.array(vector_stacked)[np.where(nmnqm == x)[0]]  
                 pr_num_qm_var.append(this_numqm)
             
             rebinned_var.append(pr_num_qm_var)
            
        return rebinned_var
          
    

    def rot_qmmm_dip(self, d, **kwargs):
        ''' rot_qmmm_dip '''
        if d is None:
            return 1
        return [kwargs['r_qmmm_poles']]
    
    
    def rot_mmmm_dip(self, d, **kwargs):
        ''' rot_mmmm_dip 
            (Identical for each QMMM configuration...) '''
        if d is None:
            return 1
        return [self.r_mm_poles]
    
    
    def polediff(self, d, **kwargs):
        ''' allpdiffs: Simple vector difference ''' 
        if d is None:
            return 1
        r_qmmm_poles = kwargs['r_qmmm_poles']
        mask = kwargs['mask']
        # making sure were comparing  the right poles
        assert(np.shape(r_qmmm_poles) ==  
               np.shape(self.r_mm_poles[~mask[::3]]))
        return [r_qmmm_poles - self.r_mm_poles[~mask[::3]]]
 
           
    def poleangle(self, d, **kwargs):
        ''' allangles: Angles between unrotated poles. 
            Verified that the rotation 
            is not necessary (aka we are comparing the right poles) '''
        if d is None:
            return 1  # Return number of outputs for list init.
        qmmm_poles = kwargs['qmmm_poles']
        mask = kwargs['mask']
        angles = np.zeros(len(qmmm_poles))
        for a, (qmmm, mmmm) in enumerate(zip(qmmm_poles, 
                                             self.mm_poles[~mask[::3]])):
            angles[a] = np.degrees(np.arctan2(
                        np.linalg.norm(np.cross(qmmm, mmmm)),
                        np.dot(qmmm, mmmm)))
        return [angles]
 
    
    def pnormdiff(self, d, **kwargs):
        ''' allnorms '''
        if d is None:
            return 1
        qmmm_poles = kwargs['qmmm_poles']
        mask = kwargs['mask']
      
        return [np.linalg.norm(qmmm_poles, axis=1) -
                np.linalg.norm(self.mm_poles[~mask[::3]], axis=1)]
  
    
    def pnormdiffpct(self, d, **kwargs):
        ''' allnormspct '''
        if d is None:
            return 1
        qmmm_poles = kwargs['qmmm_poles']
        mask = kwargs['mask']
      
        #norms = np.linalg.norm(qmmm_poles, axis=1) -\
        #        np.linalg.norm(self.mm_poles[~mask[::3]], axis=1)
                 
        #return [100 * norms / (np.linalg.norm(self.mm_poles[~mask[::3]], axis=1) * self.num_cluster)]

        norms = (np.linalg.norm(qmmm_poles, axis=1) -
                np.linalg.norm(self.mm_poles[~mask[::3]], axis=1)) /\
                np.linalg.norm(self.mm_poles[~mask[::3]], axis=1)
                #self.scme_dip

        return [100 * norms]

    def qpnormdiffpct(self, d, **kwargs):
        ''' qpolenormdiffs '''
        if d is None:
            return 1
        qmmm_qp = kwargs['qmmm_qpoles']
        mask = kwargs['mask']
        
        qmmm_norms = np.array([np.linalg.norm(x) for x in qmmm_qp])
        mm_norms = np.array([np.linalg.norm(x) 
                            for x in self.mm_qpoles[~mask[::3]]])
            
        #out = 100 * (qmmm_norms - mm_norms) / (self.qp_static_norm)
        out = 100 * (qmmm_norms - mm_norms) / (mm_norms)
                 
        #return [100 * (qmmm_norms - mm_norms) / mm_norms]
        return [out]
    
    def dens_and_qmnn(self, d, **kwargs):
        ''' allmindists, allscmelike_dens, allqm_nns, global_mindist '''
        if d is None:
            return 4
        qmmm_poles = kwargs['qmmm_poles']
        
        qm_opos = d['qm_positions'][::3]
        mm_opos = d['mm_positions'][::3] 
        mindist = np.zeros(len(qmmm_poles))  
        
        scmelike_dens = np.zeros(len(qmmm_poles))
        qm_nns = np.zeros(len(qmmm_poles))
        for m, mm in enumerate(mm_opos):
            dists = np.linalg.norm(qm_opos - mm, axis=1)  
            mindist[m] = np.min(dists)
            scmelike_dens[m] = np.sum(np.exp(-dists) / dists**3)
            qm_nns[m] = np.sum(dists < self.nndist)
        
        global_mindist = np.min(mindist)
        
        return mindist, scmelike_dens, qm_nns, [global_mindist]


    
    def bond_distortion(self, d, **kwargs):
        if d is None:
            return 2
        
        qmmm_poles = kwargs['qmmm_poles']
        mm_molpos = d['mm_positions']
        
        b1_scme = self.b1_scme
        b2_scme = self.b2_scme
        b3_scme = self.b3_scme
        
        distortion = np.zeros(len(d['mm_dipoles']))
        distortion_bonds = np.zeros(len(d['mm_dipoles']))
        for m, p in enumerate(qmmm_poles):
            m_pos = mm_molpos[m * 3:m * 3 + 3]
            b1 = np.linalg.norm(m_pos[0]- m_pos[1])
            b2 = np.linalg.norm(m_pos[0]- m_pos[2])
            b3 = np.linalg.norm(m_pos[1]- m_pos[2])
                
            distortion[m] = np.abs(b1 - b1_scme) +\
                            np.abs(b2 - b2_scme) +\
                            np.abs(b3 - b3_scme)
                            
            distortion_bonds[m] = b1 - b1_scme + b2 - b2_scme     
        
        return distortion, distortion_bonds

    
    def delta_eint(self, d, **kwargs):
        ''' alldeints aka Delta Delta E. 
            Return repeated values for correlation plots, but also 
            Singular values for proper histograms. '''
        if d is None:
            return 2
       
        if 'mm_dipoles' in d.keys():
            delta_eint = np.zeros(len(d['mm_dipoles']))
        else: 
            delta_eint = np.zeros(len(d['mm_positions']) // 3)
        # repeating the same total value for this conf for each pole
        for m in range(len(delta_eint)):  
            delta_eint[m] = 1e3 * (d['energies']['eint'] 
                                   - self.this_mm_eint) / self.num_cluster
        
        return delta_eint, [delta_eint[0]]

            
    def qm_dipdiff(self, d, **kwargs):
        ''' QM Dipole vs MM equivalent '''
        if d is None:
            return 2
        
        mask = kwargs['mask']
        numqm = len(d['qm_idx']) // 3
        if numqm != 1:
            return [], []
        
        qm_pole = - d['qm_dipole'] / Bohr 
        
        # same pole as the pure QM one
        mm_equivalent_pole = self.mm_poles[mask[::3]][0]
        qm_angle = np.degrees(np.arctan2(np.linalg.norm(np.cross(qm_pole, mm_equivalent_pole)), 
                                         np.dot(qm_pole, mm_equivalent_pole)))
        qm_normdiff = np.linalg.norm(qm_pole) - np.linalg.norm(mm_equivalent_pole)
        
        # these were appended, not extended so return them in lists.
        return [qm_angle], [qm_normdiff]  

    
    def rmsd_diff(self, d, **kwargs):
        ''' RMSD Distortion of molecule n vs the scme geometry '''
        if d is None:
            return 1

        qmmm_pos = np.copy(d['mm_positions'].reshape((-1, 3, 3)))
        scme_pos = self.scme_pos
        m2 = scme_pos - rmsd.centroid(scme_pos)
        distortion = np.zeros(len(d['mm_dipoles']))
        for i, m1 in enumerate(qmmm_pos):
            m1 -= rmsd.centroid(m1)
            U = rmsd.kabsch(m1, m2)
            m1 = np.dot(m1, U)
            distortion[i] = rmsd.rmsd(m1, m2)
        
        return [distortion]
    
    def dde_vs_2qmdist(self, d, **kwargs):
        ''' Gets QM-QM distance and ddE for 2 QM runs. Not part of the
            standard functions. Currently. ''' 
        if d is None:
            return 2
                
        qmidx = d['qm_idx']
        if len(qmidx) != 6:
            return [[np.nan], [np.nan]]
        
        qmopos = d['positions'][qmidx[::3]]
        qmodist = np.linalg.norm(qmopos[0] - qmopos[1])
        
        de = self.de_sep2018(d, **kwargs['DATA']['JOB_INFO'])
        
        # update self.this_mm_eint
        self.get_mm(kwargs['DATA'], 'LOL')
        
        # make DeltaDeltaE
        self.num_cluster = len(kwargs['DATA']['JOB_INFO']['atoms']) // 3
        ddE = 1e3 * (de[0] - self.this_mm_eint) / self.num_cluster
        
        return [[qmodist], [ddE]]        
        
        
