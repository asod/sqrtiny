import numpy as np 
from tools.adf import qmmic
def wcut(atoms, maxmols, exidx, qmidx, ctidx=[], nm=3,
                 rearrange=True, rmax=np.inf):
    ''' Cutout N closest water mols. 
        based on the MDUtils.expand_ct_qm

        rmax:      radius to expand to
        exidx:     idx of atoms to expand around. AKA CUTOUT
        qmidx:     original qm atoms idx.
        ctidx:     counter ion idx. Use [] if none.
        nm:        atoms per solvent molecule
        rearrange: rearranges atoms object to QM,CT,MM...
        maxmols:   stop expanding before this atom # even if rmax is
                   not yet reached.
                   Set rmax to something long if you want to only stop
                   on maxmols.

        All idxs should be lists, not arrays

        Atoms are wrapped around centered qm+ction idxs

        Returns qmtags array:
            2: Original qmidx
            1: New QM atoms
            0: MM atoms
            3: CT ions

        WIP:
        The maxmols method have not yet been tested on CT ion systems
        and have only been tested on ordering QM,MM...

        '''

    atoms = qmmic(atoms, qmidx + ctidx, nm) # now qmidx is list!
    atoms.center()

    c = atoms.constraints
    atoms.constraints = []

    mm_mask = np.ones(len(atoms), bool)
    mm_mask[qmidx] = False

    qmtags = np.zeros(len(atoms))
    qmtags[qmidx] += 2  # original qm
    qmtags[ctidx] += 3  # counter ions

    pos = atoms.get_positions()

    new_idxs = []
    newmols = 0

    for ex in exidx:
        r = atoms[ex].position - pos
        d = np.linalg.norm(r, axis=1)
        if maxmols < np.inf:  # Rearrange atoms closest to center atom
            assert(len(exidx) == 1),\
                'Only works for single-atom-center. WIP'

            mol_sorter = np.argsort(d[mm_mask][::nm]) * nm + len(qmidx)

            atm_sorter = sum([list(range(x, x + nm)) for x in mol_sorter], [])

            atoms = atoms[qmidx] + atoms[atm_sorter]  # WILL rearrange
            # update all of the used vars.
            pos = atoms.get_positions()
            r = atoms[ex].position - pos
            d = np.linalg.norm(r, axis=1)

        within = d < rmax
        if maxmols < np.inf:  # XXX WIP: how about CT ions??
            new_idxs = np.where(within)[0][:maxmols * nm + len(qmidx)]
        else:
            for w in np.where(within)[0]:
                if (w in mmidx[::nm]) and (w not in new_idxs):
                    for a in range(nm):
                        new_idxs.append(w+a)
    qmtags[new_idxs] += 1  # new ones
    qmtags[qmidx] = 2  # original qm. WIP: only works if QM is first and thus avoids rearrange

    atoms.set_tags(qmtags)

    # If maxmols is less than inf, atoms have already been rearranged
    if rearrange and (maxmols == np.inf):
        new_mm_idx = np.where(qmtags == 0)[0]
        atoms = atoms[qmidx] + atoms[new_idxs] + atoms[ctidx] +\
                atoms[new_mm_idx]

        qmtags = np.concatenate((qmtags[qmidx],
                                 qmtags[new_idxs],
                                 qmtags[ctidx],
                                 qmtags[new_mm_idx]))

    return atoms, qmtags


