""" Run from root dir with mpirun -np X python -m EXAMPLES/step_par_adf """

from tools.adf import ADF
import glob
from mpi4py import MPI

world = MPI.COMM_WORLD

trajs = ['/home/asod/Dropbox/HI/SCME/2018_September/liquidTIP4P/tip4p_256MolBox.traj']
mmmm_adf = ADF(trajs, sample_idx=[], start=50, step=5, pbc=True, world=world, sample_within=3.37, 
               maxmols=30, tetra_threshold=3.1522)
mmmm_adf.run_step_parallel()


#trajs = ['scme_pbc_all_50steptherm.traj']
trajs = sorted(glob.glob('/home/asod/Dropbox/HI/SCME/RDFtrajs/more/*_01.traj'))
mmmm_adf = ADF(trajs, sample_idx=list(range(0, 512, 20)), pbc=True, world=world, sample_within=3.22, 
               maxmols=30, tetra_threshold=3.19559)

# empty means ALL
mmmm_adf.run_traj_parallel()

