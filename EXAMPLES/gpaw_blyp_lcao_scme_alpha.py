from jinerate import Templater
import numpy as np

t = Templater(calctype='PES', pestype='alpha', mode='lcao')
t.set_defaults(xc='BLYP')
t.megadict['gpaw_args']['basis'] = 'BLYP.tzp'
t.set_paths()
t.megadict['vac'] = 7.
t.megadict['scme_param']['g'] = 0.66
for qmidx, conf in zip(([0, 1, 2], [3, 4, 5]), ('_BLYPtzpSCME', '_SCMEBLYPtzp' )):
    t.megadict['qmidx'] = qmidx
    t.make(t.rootdir+t.xyzdir+'scme_dimer.xyz', extra=conf)
