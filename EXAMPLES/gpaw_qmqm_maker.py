from jinerate import Templater
import numpy as np

 
for PES in ['roo', 'ooh', 'alpha', 'ooxh', 'hoox']:
    for func in ['BLYP', 'PBE']:
        for mode in ['grid', 'lcao']:
            t = Templater(calctype='PES', pestype=PES, mode=mode)
            t.set_defaults(xc=func)
            t.tfile = 't_gpaw_pureqm_pes.py'
            t.megadict['extrapath'] = 'QMQM'
            t.set_paths()
            t.megadict['vac'] = 4.
            t.make(t.rootdir+t.xyzdir+'scme_dimer.xyz')


