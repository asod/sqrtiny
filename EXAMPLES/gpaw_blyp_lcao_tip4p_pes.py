from jinerate import Templater
import numpy as np

for pes in ['roo', 'alpha', 'ooh', 'hoox', 'ooxh']:
    t = Templater(calctype='PES', pestype=pes, mode='lcao')
    t.set_defaults(xc='BLYP', mmcalc='TIP4P', mmcalc_args='(rc=100.)')
    t.megadict['gpaw_args']['basis'] = 'BLYP.tzp'
    t.set_paths()
    t.megadict['vac'] = 7.
    for qmidx, conf in zip(([0, 1, 2], [3, 4, 5]), ('BLYPtzpTIP4P', 'TIP4PBLYPtzp' )):
        t.megadict['qmidx'] = qmidx
        t.make(t.rootdir + t.xyzdir + 'scme_dimer.xyz', extra=conf)
