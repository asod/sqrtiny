from jinerate import Templater
from tools.crystal_pp import move_to_subdirs, inject_def2tzvp, inject_other
import glob

''' Script that uses the templater to make cluster calculation input scripts 
    for crystal and amber, and puts the input files in separate directories, 
    to avoid overwriting of crystal/amber IO files across the differerent runs. 
''' 

# init
for pes in ['roo', 'alpha', 'ooh', 'hoox', 'ooxh']:
    pestype = pes
    t = Templater(program='crystal', calctype='PES', pestype=pestype)

    # Choose functional
    xc = 'B3LYP'

    for lj in ['int_tip3p', 'int_b3lyp']:
        t.set_defaults(xc=xc, lj=lj)

        # Use correct template
        t.set_templatefile('t_crystal_pes.py')

        t.set_paths()

        # Generate input scripts
        for qmidx, conf in zip(([0, 1, 2], [3, 4, 5]), ('_B3LYPTIP3P', '_TIP3PB3LYP')):
            tag = '_'+lj.replace('int', 'lj')
            conf += tag
            print(conf)
            t.megadict['qmidx'] = qmidx
            t.megadict['extra_tag'] = tag
            t.make(t.rootdir + t.xyzdir + 's22_dimer.xyz', extra=conf)

    #inject_def2tzvp(t.rootdir + '/RUNS/PES_' + pestype + '/crystal/' + xc) 
    inject_other(t.rootdir + '/RUNS/PES_' + pestype + '/crystal/' + xc) 

