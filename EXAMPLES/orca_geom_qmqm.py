from jinerate import Templater

for basis in ['def2-SVP', 'def2-TZVP']:
    t = Templater(calctype='Geometry', program='orca', mode='lcao', basis=basis)
    t.orcablocks = '%scf Convergence verytight \nmaxiter 300 end\n\n%pal nprocs 16 end\n\n'
    t.set_defaults(xc='BLYP')
    t.tfile = 't_orca_qmqm_geometry.py'
    t.megadict['extrapath'] = 'QMQM_' + basis
    t.set_paths()
    t.make()
