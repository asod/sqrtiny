""" Run from root dir with mpirun -np X python -m EXAMPLES/step_par_adf """

from tools.adf import ADF
import glob
from mpi4py import MPI

world = MPI.COMM_WORLD

#trajs = ['/home/asod/Dropbox/HI/SCME/2018_September/liquidTIP4P/tip4p_256MolBox.traj']

# 3 ps re-eq, on concated, wrapped trajs made with WrapConcat.py in pythonlib
trajs = sorted(glob.glob('/home/asod/Dropbox/HI/SCME/2019_Jan/blyp/BLYPSCME_PBC_??_00-0?.traj'))
qmmm_adf = ADF(trajs, [0], start=60,pbc=True, world=world, qmidx=[0], sample_within=3.30, 
               maxmols=20, tetra_threshold=3.31291)
qmmm_adf.run_traj_parallel()


# 3 ps re-eq, on concated, wrapped trajs made with WrapConcat.py in pythonlib
trajs = sorted(glob.glob('/home/asod/Dropbox/HI/SCME/2019_Jan/weblol/PBESCME_PBC_??_00-0?.traj'))
qmmm_adf = ADF(trajs, [0], start=60,pbc=True, world=world, qmidx=[0], sample_within=3.31, 
               maxmols=20, tetra_threshold=3.18559)
qmmm_adf.run_traj_parallel()

# PBETIP4P
trajs = sorted(glob.glob('/home/asod/Dropbox/HI/SCME/2018_September/liquidPBETIP4p/PBET4P_??.traj'))
qmmm_adf = ADF(trajs, [0], start=60,pbc=True, world=world, qmidx=[0], sample_within=3.35, 
               maxmols=20, tetra_threshold=3.05405)
qmmm_adf.run_traj_parallel()
