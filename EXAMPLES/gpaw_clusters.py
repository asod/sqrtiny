from jinerate import Templater

# GPAW RPBE/SCME grid mode
t = Templater()
t.set_defaults(xc='RPBE')
t.tfile = "t_gpaw_clusters.py"
# change something, e.g.
t.megadict['scme_param']['damping_value'] = 0.7155
t.megadict['gpaw_args']['nbands'] = - 5
t.set_paths()
t.make()


# GPAW PBE/LCAO//TIP4P
t = Templater(mode='lcao')
t.set_defaults(xc='PBE', mmcalc='TIP4P(rc=100.)')
t.tfile = "t_gpaw_clusters.py"
t.set_paths()
t.make()
