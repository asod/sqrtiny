from jinerate import Templater

# Not using the libxcvdw backend. Slow.

t = Templater()
t.set_defaults(xc='vdW-DF2')
t.tfile = "t_gpaw_scme2019_clusters_grid.py"
t.megadict['gpaw_args']['nbands'] = - 5
t.megadict['gpaw_args']['occupations'] = 'FermiDirac(0.01)'
t.megadict['gpaw_args']['mixer'] = 'Mixer(0.03, 10, 50)'
t.megadict['gpaw_args']['eigensolver'] = 'CG(niter=20, rtol=0.08)'
t.megadict['gpaw_args']['setups'] = 'vdW-DF2'
t.set_paths()
t.make()

