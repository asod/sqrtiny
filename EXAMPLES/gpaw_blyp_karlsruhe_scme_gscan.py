from jinerate import Templater
import numpy as np

basis={'H': 'BLYP.Def2-TZVPD.sz',
       'O': 'BLYP.Def2-TZVPD.sz'}


t = Templater(calctype='PES', pestype='roo', mode='lcao')
t.set_defaults(xc='BLYP')
t.megadict['gpaw_args']['mode'] = 'lcao'
t.megadict['gpaw_args']['basis'] = basis
t.megadict['extrapath'] = 'BLYP-def2-TZVPD'
t.set_paths()
t.megadict['vac'] = 7.
for g in np.arange(0.36, 0.7, 0.02):
    t.megadict['scme_param']['damping_value'] = g
    t.megadict['qmidx'] = [0, 1, 2]
    t.make(t.rootdir+t.xyzdir+'scme_dimer.xyz', extra='_BLYPSCME_Def2-TZVPD_g{0:03.2f}'.format(g))
    t.megadict['qmidx'] = [3, 4 ,5]
    t.make(t.rootdir+t.xyzdir+'scme_dimer.xyz', extra='_SCMEBLYP_Def2-TZVPD_g{0:03.2f}'.format(g))
