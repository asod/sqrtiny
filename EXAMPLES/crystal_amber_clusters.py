from jinerate import Templater
from tools.crystal_pp import move_to_subdirs, inject_def2tzvp
import glob

''' Script that uses the templater to make cluster calculation input scripts 
    for crystal and amber, and puts the input files in separate directories, 
    to avoid overwriting of crystal/amber IO files across the differerent runs. 
''' 

# init
t = Templater(program='crystal')

# Use correct template
t.set_templatefile('t_crystal_amber_clusters.py')

# Choose functional
xc = 'PBE'

# Send AMBER ASE interface arguments to input generator and prepare  
amber_args = {'amber_exe':'mpirun -np 1 /data/software/amber16/bin/sander.MPI -O ',
             'infile': 'mm.in', 'outfile': 'mm.out', 'topologyfile':'dry.prmtop',
              'incoordfile': 'dry.inpcrd'}

t.set_defaults(xc=xc, mmcalc='Amber', mmcalc_args=amber_args)

# Generate input scripts
t.make()

# move files around a bit
move_to_subdirs('./RUNS/WaterClusters/crystal/' + xc)

# write the basis file for crystal in each subdir
rundirs = glob.glob('./RUNS/WaterClusters/crystal/' + xc + '/*/')
for d in rundirs:
    inject_def2tzvp(d)
