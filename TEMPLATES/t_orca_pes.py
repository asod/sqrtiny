import numpy as np
from ase.io import read
from ase.calculators.orca import ORCA
import os
from ase.io.trajectory import TrajectoryWriter
from ase.parallel import rank
from ase.calculators.tip3p import TIP3P, epsilon0 as eps3, sigma0 as sig3
from ase.calculators.tip4p import TIP4P, epsilon0 as eps4, sigma0 as sig4
from ase.calculators.qmmm import EIQMMM, Embedding, LJInteractions

# add my own tools
import sys
sys.path.append('{{ rootdir|string() }}')
from pesmakers import set_{{ geom }} as set_geom 

    
def write_out(fpath, data):
    num_column = len(data)
    with open(fpath, 'a') as f:
        f.write(('{:>18.10f}' * num_column).format(*data))
        f.write('\n')


# Define params
qmidx = {{ qmidx|string() }}
vac = {{ vac }}
kwargs = {{ orca_args }}
xc = '{{ xc }}'
basis = '{{ basis }}'


if 0 in qmidx:
    conf = xc + '_'+ basis + '_{{mmcalc|string() }}'
    movemask = [3, 4, 5]
elif 3 in qmidx:
    conf = '{{mmcalc|string() }}_' + xc + '_'+ basis
    movemask = [0, 1, 2]


tag = conf 

points = {{ points }}


the_file = "{{ xyzfile }}"
atoms = read(the_file)

atoms.center(vacuum=vac)
cell = atoms.cell
    
# monomer energy:
mono = atoms[qmidx]
mono.calc = ORCA(label=tag + '_mono',
                 orcasimpleinput=kwargs['orcasimpleinput'],
                 orcablocks=kwargs['orcablocks'])

try:
    e_mono = mono.get_potential_energy()
except:
    e_mono = np.NaN  # then try to get it otherwise..

writer = TrajectoryWriter(tag+'.traj', 'w', atoms=atoms)
e_mm = np.zeros_like(points)
for s, d in enumerate(points):
    set_geom(atoms, d, movemask=movemask)


    emb = {{ embedding|string() }}
    interaction = {{ interaction|string() }}
    atoms.calc = EIQMMM(selection=qmidx,
                        qmcalc=ORCA(label=tag+'_d{0:07.4f}.out'.format(d),
                                    orcasimpleinput=kwargs['orcasimpleinput'],
                                    orcablocks=kwargs['orcablocks']),
                        mmcalc={{ mmcalc }}(rc=np.inf),
                        interaction=interaction,
                        embedding=emb,
                        output='outputfiles/'+\
                                tag+'_{0:07.4f}QMMM.log'.format(d))
    try:
        e = atoms.get_potential_energy()
    except:
        if rank == 0:
            print('Calculation failed at distance {0:g}!'.format(d))
        e = np.NaN  # if not converged 

    data = (d, e, e_mono)

    if rank == 0:
        write_out(tag+'.dat', data)

    writer.write(atoms)
