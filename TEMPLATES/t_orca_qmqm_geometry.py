import numpy as np
from ase.constraints import FixBondLengths
from ase.io import Trajectory, read
from ase.calculators.orca import ORCA
from ase.optimize.fire import FIRE

import sys
sys.path.append('{{ rootdir|string() }}')

path = '{{ rundir|string() }}'

the_file = '{{ xyzfile }}'
kwargs = {{ orca_args }}
xc = '{{ xc }}'
basis = '{{ basis }}'

tag = the_file.split('/')[-1].split('.xyz')[0]
tag += '{{ extra }}'


atoms = read(the_file)

# fix all bonds
c1 = FixBondLengths([(3 * i + j, 3 * i + (j + 1) % 3)
                     for i in range(len(atoms) // 3)
                     for j in [0, 1, 2]])

atoms.constraints = c1



atoms.calc = ORCA(label=path + '/outputfiles/' + tag + '.out',
                  orcasimpleinput=kwargs['orcasimpleinput'],
                  orcablocks=kwargs['orcablocks'])

opt = FIRE(atoms, logfile=path + '/' + tag + '_opt.log')
traj = Trajectory(path + '/' + tag + '.traj', 'w', atoms)
opt.attach(traj.write, interval=1)
opt.run(fmax={{ fmax }}, steps={{ geom_steps }})

