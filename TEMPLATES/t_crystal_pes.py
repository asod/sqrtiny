from ase.units import Hartree, Bohr
import numpy as np
from ase.io import read

import os
from ase.io.trajectory import TrajectoryWriter
from ase.parallel import rank
from ase.calculators.tip3p import TIP3P, sigma0 as sig3, epsilon0 as eps3
from ase.calculators.tip4p import TIP4P, sigma0 as sig4, epsilon0 as eps4
from ase.calculators.qmmm import EIQMMM, Embedding, LJInteractions
from ase.calculators.crystal import CRYSTAL

import itertools as it

# add my own tools
import sys
sys.path.append('{{ rootdir|string() }}')
from pesmakers import set_{{ geom }} as set_geom 
from waterclusters import mm_coulomb
    
def write_out(fpath, data):
    num_column = len(data)
    with open(fpath, 'a') as f:
        f.write(('{:>20.12f}' * num_column).format(*data))
        f.write('\n')

path = '{{ rundir|string() }}'

# Define params
qmidx = {{ qmidx }}
mmcalc = {{ mmcalc | string }}
cryst_args = {{ cryst_args }}


if 0 in qmidx:
    conf = cryst_args['xc'] + mmcalc().name
    movemask = [3, 4, 5]
elif 3 in qmidx:
    conf = mmcalc().name + cryst_args['xc']
    movemask = [0, 1, 2]


tag = conf + '{{ extra_tag }}'

points = {{ points }}

the_file = "{{ xyzfile }}"
atoms = read(the_file)
atoms.center()

  
# monomer energy:
mono = atoms[qmidx]
mono.calc = CRYSTAL(**cryst_args)
try:
    e_mono = mono.get_potential_energy()
except:
    e_mono = np.NaN  # then try to get it otherwise..


# Lennard-Jones:
#LJ from J.Comput. Chem 2005,26,1270
epsOO = 0.0082392
sigOO = 3.33
epsHH = 0.0013009
sigHH = 1.06
epsCC = 0.026078
sigCC = 4.02
epsNN = 0.0056373
sigNN = 3.86

parameters = {'O': (epsOO, sigOO),
              'H': (epsHH, sigHH),
              'N': (epsNN, sigNN),
              'C': (epsCC, sigCC)}

def lorenz_berthelot(p):
    combined = {}
    for comb in it.product(p.keys(), repeat=2):
        combined[comb] = ((p[comb[0]][0] * p[comb[1]][0])**0.5,
                          (p[comb[0]][1] + p[comb[1]][1]) / 2.)
    return combined

combined = lorenz_berthelot(parameters)

int_b3lyp = LJInteractions(combined)
int_tip3p = LJInteractions({('O', 'O'): (eps3, sig3)})
int_tip4p = LJInteractions({('O', 'O'): (eps4, sig4)})


writer = TrajectoryWriter(path+tag+'.traj', 'w', atoms=atoms)
e_mm = np.zeros_like(points)
for s, d in enumerate(points):
    set_geom(atoms, d, movemask=movemask)
    mmatoms = atoms[movemask]
    mmatoms.calc = mmcalc()
    e_mm_coul = mm_coulomb(mmatoms)
    atoms.calc = EIQMMM(selection=qmidx,
                        qmcalc=CRYSTAL(**cryst_args),
                        mmcalc=mmcalc(),
                        interaction={{ lj }},
                        output=path+'outputfiles/'+\
                               tag+'_{0:07.4f}QMMM.log'.format(d))
    try:
        e = atoms.get_potential_energy()
    except:
        if rank == 0:
            print 'Calculation failed at distance {0:g}!'.format(d)
        e = np.NaN  # if not converged 

    data = (d, e, e_mono, e_mm_coul)

    if rank == 0:
        write_out(path+tag+'.dat', data)

    writer.write(atoms)
